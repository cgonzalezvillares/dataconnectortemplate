import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataConnectorService } from '../data-connector/data-connector.service';
import { HttpService } from '../http-service.service';

@NgModule({
  imports: [CommonModule],
  providers: [DataConnectorService, HttpService]
})
export class DataConnectorModule {}
