import { TestBed, inject } from '@angular/core/testing';

import { UsersService } from './users.service';
import { HttpClient } from '@angular/common/http';

describe('UsersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsersService, HttpClient]
    });
  });

  it('should be created', inject([UsersService, HttpClient], (service: UsersService) => {
    expect(service).toBeTruthy();
  }));
});
