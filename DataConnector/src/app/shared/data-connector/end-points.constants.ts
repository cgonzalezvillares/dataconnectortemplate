import { HTTP_VERBS } from './http-verbs.constants';

export const END_POINTS = {
  USER: {
    DATA_SOURCES: {
      HTTP: {
        URL: 'https://jsonplaceholder.typicode.com/users/:id',
        VERB: HTTP_VERBS.GET
      },
      MOCK: { URL: 'assets/mocks/user.json' }
    }
  },
  USERS: {
    DATA_SOURCES: {
      HTTP: {
        URL: 'https://jsonplaceholder.typicode.com/users',
        VERB: HTTP_VERBS.GET
      },
      MOCK: { URL: 'assets/mocks/users.json' }
    }
  },
  NEW_USER: {
    DATA_SOURCES: {
      HTTP: {
        URL: 'https://jsonplaceholder.typicode.com/posts',
        VERB: HTTP_VERBS.POST
      },
      MOCK: { URL: 'assets/mocks/user.json' }
    }
  }
};
