import { TestBed, inject } from '@angular/core/testing';

import { DataConnectorService } from './data-connector.service';
import { HttpService } from './http-service.service';

describe('DataConnectorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataConnectorService, HttpService]
    });
  });

  it('should be created', inject([DataConnectorService, HttpService], (service: DataConnectorService) => {
    expect(service).toBeTruthy();
  }));
});
