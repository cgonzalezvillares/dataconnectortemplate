import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users/users.service';

@Component({
  selector: 'dc-users-frame',
  templateUrl: './users-frame.component.html',
  styleUrls: ['./users-frame.component.scss']
})
export class UsersFrameComponent implements OnInit {
  public users: Array<any>
  public user: any;
  constructor(private usersService: UsersService) { }

  ngOnInit() {
    console.log('UsersFrame init');
  }

  load() {
    console.log('load');
    this.usersService.getUsers(users => this.users = users);
    this.usersService.getUserById({id: 1},user => this.user = user);
  }

  create() {
    const newUserData = {
      name: 'Pepe',
      telf: '23423423423'
    }
    this.usersService.newUser( newUserData
      ,user => this.user = user);

  }

}
