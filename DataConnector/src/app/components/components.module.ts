import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersFrameComponent } from './users-frame/users-frame.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    UsersFrameComponent],
    exports: [UsersFrameComponent]
})
export class ComponentsModule { }
