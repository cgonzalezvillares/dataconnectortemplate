import { TestBed, inject } from '@angular/core/testing';

import { HttpService } from './http-service.service';
import { HttpClient } from '@angular/common/http';

describe('HttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpService, HttpClient]
    });
  });

  it('should be created', inject([HttpService, HttpClient], (service: HttpService) => {
    expect(service).toBeTruthy();
  }));
});
