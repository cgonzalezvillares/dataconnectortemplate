import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { END_POINTS } from './end-points.constants';
import { APP_AVALIABLE_DATA_SOURCES } from '../../shared/data-connector/avaliable-data-sources.constants';
import { HTTP_VERBS } from './http-verbs.constants';
import { HttpService } from '../http-service.service';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { map, filter, scan } from 'rxjs/operators';
import { JSONP_ERR_WRONG_RESPONSE_TYPE } from '@angular/common/http/src/jsonp';

@Injectable({
  providedIn: 'root'
})
export class DataConnectorService {
  constructor(private httpService: HttpService) {}

  getUsers() {
    /**
     * TODO convert request
     */ return this.getData(END_POINTS.USERS).pipe(
      map(response => {
        /** TODO convert response */
        return response.map(user => {
          return { id: user.id, name: user.name };
        });
      })
    );
  }

  getUser(data) {
    /**
     * TODO convert request
     */ return this.getData(END_POINTS.USER, data).pipe(
      map(user => {
        /** TODO convert response */
        return { id: user.id, name: user.name };
      })
    );
  }

  newUser(data) {
    /**
     * TODO convert request
     */
    return this.getData(END_POINTS.NEW_USER, data).pipe(
      map(response => {
        /** TODO convert response */
        return response.map(user => {
          return { id: user.id, name: user.name };
        });
      })
    );
  }

  getData(endPoint, data?) {
    if (
      environment.APP_ACTIVE_DATA_SOURCE === APP_AVALIABLE_DATA_SOURCES.HTTP
    ) {
      if (endPoint.DATA_SOURCES.HTTP.VERB === HTTP_VERBS.GET) {
        let url: string = endPoint.DATA_SOURCES.HTTP.URL;
        if (data) {
          Object.keys(data).map(key => {
            url = url.replace(`:${key}`, data[key]);
          });
        }
        return this.httpService.getHttp(url, data, {
          headers: this.getHeaders()
        });
      } else if (endPoint.DATA_SOURCES.HTTP.VERB === HTTP_VERBS.POST) {
        return this.httpService.postHttp(endPoint.DATA_SOURCES.HTTP.URL, data, {
          headers: this.getHeaders()
        });
      }
    } else if (
      environment.APP_ACTIVE_DATA_SOURCE === APP_AVALIABLE_DATA_SOURCES.MOCK
    ) {
      return this.httpService.getHttp(endPoint.DATA_SOURCES.MOCK.URL, data);
    }
  }

  getHeaders() {
    return new HttpHeaders({
      Autentication: 'publickey'
    });
  }
}
