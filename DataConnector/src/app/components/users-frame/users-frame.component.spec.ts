import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersFrameComponent } from './users-frame.component';
import { UsersService } from '../../services/users/users.service';

class  usersServiceMock {
  
}

describe('UsersFrameComponent', () => {
  let component: UsersFrameComponent;
  let fixture: ComponentFixture<UsersFrameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersFrameComponent, {provide: UsersService, useClass: usersServiceMock } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
