import { Injectable } from '@angular/core';
import { DataConnectorService } from '../../shared/data-connector/data-connector.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private dataConnector: DataConnectorService) {}

  getUserById(data, success?: Function, error?: Function) {
    console.log(data);
    this.dataConnector.getUser(data).subscribe(
      response => {
        if (success) {
          success(response);
        }
      },
      () => {
        if (error) {
          error();
        }
      }
    );
  }

  getUsers(success?: Function, error?: Function) {
    this.dataConnector.getUsers().subscribe(
      response => {
        if (success) {
          success(response);
        }
      },
      () => {
        if (error) {
          error();
        }
      }
    );
  }

  newUser(data, success?: Function, error?: Function) {
    this.dataConnector.newUser(data).subscribe(
      response => {
        if (success) {
          success(response);
        }
      },
      () => {
        if (error) {
          error();
        }
      }
    );
  }
}
