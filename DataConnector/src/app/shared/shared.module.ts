import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataConnectorService } from './data-connector/data-connector.service';
import { HttpService } from './http-service.service';
import { DataConnectorModule } from './data-connector/data-connector.module';

@NgModule({
  imports: [CommonModule, DataConnectorModule],
  providers: []
})
export class SharedModule {}
